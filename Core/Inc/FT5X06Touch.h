/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FT5X06_H
#define __FT5X06_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"


#define FT5X06_TOUCH_POINTS 5
#define GFG_POINT_READ_BUF 33 /*  (3 + 6 * (FT5X06_TOUCH_POINTS))   */

/* Exported functions prototypes ---------------------------------------------*/
void FT5X06Init(I2C_HandleTypeDef *hi2c1);
uint8_t FT5X06_Read();
typedef struct
{
	uint8_t ChipID;
	uint8_t Enable;
	uint8_t TimerCount;

	uint8_t Count;

	uint16_t X[FT5X06_TOUCH_POINTS];
	uint16_t Y[FT5X06_TOUCH_POINTS];
	uint8_t id[FT5X06_TOUCH_POINTS];
	uint8_t Event[FT5X06_TOUCH_POINTS];
	uint8_t State;
}FT5X06_T;

FT5X06_T GetState();
void ClearState();

#ifdef __cplusplus
}
#endif

#endif /* __FT5X06_H */
