#ifndef __PRINT_SAMPLES_H
#define __PRINT_SAMPLES_H

#ifdef __cplusplus
 extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include "stdint.h"

const uint8_t *printSample();
uint16_t getPrintSampleLen();

const uint8_t *printSample2();
uint16_t getPrintSample2Len();


#endif  /* __PRINT_SAMPLES_H */