#ifndef __USB_PCL_H
#define __USB_PCL_H

#ifdef __cplusplus
 extern "C" {
#endif

#ifdef __cplusplus
}
#endif
#include "stdint.h"

const uint8_t *printSelfTest();

const uint8_t *printTest();
uint16_t getPrintTestLen() ;


#endif  /* __USB_PCL_H */
