/* Define to prevent recursive  ----------------------------------------------*/
#ifndef __USBH_PRN_H
#define __USBH_PRN_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "usbh_core.h"

void USB_Send2Printer(uint8_t* buffer, uint16_t size);

typedef enum
{
  PRN_INIT = 0,
  PRN_IDLE,
  PRN_TEST_UNIT_READY,
  PRN_READ_CAPACITY10,
  PRN_READ_INQUIRY,
  PRN_REQUEST_SENSE,
  PRN_READ,
  PRN_WRITE,
  PRN_UNRECOVERED_ERROR,
  PRN_PERIODIC_CHECK,
}
PRN_StateTypeDef;

typedef enum
{
  PRN_OK,
  PRN_NOT_READY,
  PRN_ERROR,
}
PRN_ErrorTypeDef;

typedef enum
{
  PRN_REQ_INIT = 0,
  PRN_REQ_DEVICE_ID,
  PRN_REQ_RESET,
  PRN_REQ_STATUS,
  PRN_REQ_IDLE,
  PRN_REQ_ERROR,
}
PRN_ReqStateTypeDef;

typedef struct
{
  uint8_t data[100];
}
PRN_RecData;

/* Structure for PRN process */
typedef struct _PRN_Process
{
  uint8_t              InPipe;
  uint8_t              OutPipe;
  uint8_t              OutEp;
  uint8_t              InEp;
  uint16_t             OutEpSize;
  uint16_t             InEpSize;
  PRN_StateTypeDef     state;
  PRN_ErrorTypeDef     error;
  PRN_ReqStateTypeDef  req_state;
  PRN_ReqStateTypeDef  prev_req_state;
  PRN_RecData		   device_id;
  uint32_t             timer;
  //BOT_HandleTypeDef    hbot;
  uint32_t 			   device_status_id;
}
PRN_HandleTypeDef;

#define PRN_PRINTERS 0x01 // sub-class
#define PRN_UNIDIRECTIONAL 0x01 // protocols
#define PRN_BIDIRECTIONAL 0x02

/* PRN Class Codes */
#define USB_PRN_CLASS                                  0x07U

extern USBH_ClassTypeDef  USBH_prn;
#define USBH_PRN_CLASS    &USBH_prn

/* Common APIs */
uint8_t            USBH_PRN_IsReady (USBH_HandleTypeDef *phost);

#ifdef __cplusplus
}
#endif

#endif  /* __USBH_PRN_H */