#include "usbh_prn.h"
#include <stdint.h>
/*  Projeto Criado com base no driver STM para MSC */

#define MAX_BUFFER 40000
#define USB_PKG_SIZE 64
static uint8_t printerController = 0;
static char bufferString[MAX_BUFFER];

/////////////////////////////////// MACROS ////////////////////////////////
const char pjl_Header[] = "\x1b%-12345X@PJL\n \
                              @PJL SET RESOLUTION=600\n \
                              @PJL ENTER LANGUAGE = PCL\n";
const char pjl_Footer[] = "\x1b%-12345X";
//////////////////////////////////////////////////////////////////////////

static USBH_StatusTypeDef USBH_PRN_InterfaceInit(USBH_HandleTypeDef *phost);
static USBH_StatusTypeDef USBH_PRN_InterfaceDeInit(USBH_HandleTypeDef *phost);
static USBH_StatusTypeDef USBH_PRN_Process(USBH_HandleTypeDef *phost);
static USBH_StatusTypeDef USBH_PRN_ClassRequest(USBH_HandleTypeDef *phost);
static USBH_StatusTypeDef USBH_PRN_SOFProcess(USBH_HandleTypeDef *phost);
static void USBH_PRN_Send(USBH_HandleTypeDef *phost);

USBH_ClassTypeDef USBH_prn =
    {
        "PRN",
        USB_PRN_CLASS,
        USBH_PRN_InterfaceInit,
        USBH_PRN_InterfaceDeInit,
        USBH_PRN_ClassRequest,
        USBH_PRN_Process,
        USBH_PRN_SOFProcess,
        NULL,
};

static USBH_StatusTypeDef USBH_PRN_InterfaceInit(USBH_HandleTypeDef *phost)
{
  uint8_t interface = 0U;
  USBH_StatusTypeDef status = USBH_FAIL;
  PRN_HandleTypeDef *PRN_Handle;

  interface = USBH_FindInterface(phost, phost->pActiveClass->ClassCode, PRN_PRINTERS, PRN_BIDIRECTIONAL);

  if (interface == 0xFFU) /* Not Valid Interface */
  {
    USBH_DbgLog("Cannot Find the interface for %s class.", phost->pActiveClass->Name);
    status = USBH_FAIL;
  }
  else
  {
    USBH_SelectInterface(phost, interface);

    phost->pActiveClass->pData = (PRN_HandleTypeDef *)USBH_malloc(sizeof(PRN_HandleTypeDef));
    PRN_Handle = (PRN_HandleTypeDef *)phost->pActiveClass->pData;

    if (phost->device.CfgDesc.Itf_Desc[phost->device.current_interface].Ep_Desc[0].bEndpointAddress & 0x80U)
    {
      PRN_Handle->InEp = (phost->device.CfgDesc.Itf_Desc[phost->device.current_interface].Ep_Desc[0].bEndpointAddress);
      PRN_Handle->InEpSize = phost->device.CfgDesc.Itf_Desc[phost->device.current_interface].Ep_Desc[0].wMaxPacketSize;
    }
    else
    {
      PRN_Handle->OutEp = (phost->device.CfgDesc.Itf_Desc[phost->device.current_interface].Ep_Desc[0].bEndpointAddress);
      PRN_Handle->OutEpSize = phost->device.CfgDesc.Itf_Desc[phost->device.current_interface].Ep_Desc[0].wMaxPacketSize;
    }

    if (phost->device.CfgDesc.Itf_Desc[phost->device.current_interface].Ep_Desc[1].bEndpointAddress & 0x80U)
    {
      PRN_Handle->InEp = (phost->device.CfgDesc.Itf_Desc[phost->device.current_interface].Ep_Desc[1].bEndpointAddress);
      PRN_Handle->InEpSize = phost->device.CfgDesc.Itf_Desc[phost->device.current_interface].Ep_Desc[1].wMaxPacketSize;
    }
    else
    {
      PRN_Handle->OutEp = (phost->device.CfgDesc.Itf_Desc[phost->device.current_interface].Ep_Desc[1].bEndpointAddress);
      PRN_Handle->OutEpSize = phost->device.CfgDesc.Itf_Desc[phost->device.current_interface].Ep_Desc[1].wMaxPacketSize;
    }

    PRN_Handle->state = PRN_INIT;
    PRN_Handle->error = PRN_OK;
    PRN_Handle->req_state = PRN_REQ_INIT;
    PRN_Handle->OutPipe = USBH_AllocPipe(phost, PRN_Handle->OutEp);
    PRN_Handle->InPipe = USBH_AllocPipe(phost, PRN_Handle->InEp);

    /* Open the new channels */
    USBH_OpenPipe(phost,
                  PRN_Handle->OutPipe,
                  PRN_Handle->OutEp,
                  phost->device.address,
                  phost->device.speed,
                  USB_EP_TYPE_BULK,
                  PRN_Handle->OutEpSize);

    USBH_OpenPipe(phost,
                  PRN_Handle->InPipe,
                  PRN_Handle->InEp,
                  phost->device.address,
                  phost->device.speed,
                  USB_EP_TYPE_BULK,
                  PRN_Handle->InEpSize);

    USBH_LL_SetToggle(phost, PRN_Handle->InPipe, 0U);
    USBH_LL_SetToggle(phost, PRN_Handle->OutPipe, 0U);
    status = USBH_OK;
  }
  return status;
}

static USBH_StatusTypeDef USBH_PRN_InterfaceDeInit(USBH_HandleTypeDef *phost)
{
  PRN_HandleTypeDef *PRN_Handle = (PRN_HandleTypeDef *)phost->pActiveClass->pData;

  if (PRN_Handle->OutPipe)
  {
    USBH_ClosePipe(phost, PRN_Handle->OutPipe);
    USBH_FreePipe(phost, PRN_Handle->OutPipe);
    PRN_Handle->OutPipe = 0U; /* Reset the Channel as Free */
  }

  if (PRN_Handle->InPipe)
  {
    USBH_ClosePipe(phost, PRN_Handle->InPipe);
    USBH_FreePipe(phost, PRN_Handle->InPipe);
    PRN_Handle->InPipe = 0U; /* Reset the Channel as Free */
  }

  if (phost->pActiveClass->pData)
  {
    USBH_free(phost->pActiveClass->pData);
    phost->pActiveClass->pData = 0U;
  }

  return USBH_OK;
}

static USBH_StatusTypeDef USBH_PRN_ClassRequest(USBH_HandleTypeDef *phost)
{
  PRN_HandleTypeDef *PRN_Handle = (PRN_HandleTypeDef *)phost->pActiveClass->pData;
  USBH_StatusTypeDef status = USBH_BUSY;

  uint16_t timer = 0;
  switch (PRN_Handle->req_state)
  {

  case PRN_REQ_INIT:
  case PRN_REQ_STATUS:

    if (phost->RequestState == CMD_SEND)
    {
      status = USBH_OK;
    }
    if (status == USBH_OK)
    {
      phost->gState = HOST_CLASS;
      PRN_Handle->req_state = PRN_REQ_IDLE;
    }

    break;

  case PRN_REQ_DEVICE_ID:

    break;

  case PRN_REQ_RESET:

    break;

  case PRN_REQ_IDLE:
    break;
  default:
    break;
  }

  if (status == USBH_NOT_SUPPORTED)
  {
    status = USBH_OK;
  }

  return status;
}

static USBH_StatusTypeDef USBH_PRN_Process(USBH_HandleTypeDef *phost)
{
  PRN_HandleTypeDef *PRN_Handle = (PRN_HandleTypeDef *)phost->pActiveClass->pData;
  USBH_StatusTypeDef error = USBH_BUSY;

  switch (PRN_Handle->state)
  {
  case PRN_INIT:

    if (1)
    {
#if (USBH_USE_OS == 1U)
      phost->os_msg = (uint32_t)USBH_CLASS_EVENT;
#if (osCMSIS < 0x20000U)
      (void)osMessagePut(phost->os_event, phost->os_msg, 0U);
#else
      (void)osMessageQueuePut(phost->os_event, &phost->os_msg, 0U, NULL);
#endif
#endif
    }
    else
    {
      PRN_Handle->state = PRN_IDLE;

#if (USBH_USE_OS == 1U)
      phost->os_msg = (uint32_t)USBH_CLASS_EVENT;
#if (osCMSIS < 0x20000U)
      (void)osMessagePut(phost->os_event, phost->os_msg, 0U);
#else
      (void)osMessageQueuePut(phost->os_event, &phost->os_msg, 0U, NULL);
#endif
#endif
      phost->pUser(phost, HOST_USER_CLASS_ACTIVE);
    }

    PRN_Handle->state = PRN_IDLE;
    debugPrint("USB PRINTER READY\r\n");

    break;

  case PRN_IDLE:
    error = USBH_OK;

    if (printerController)
    {
      PRN_Handle->state = PRN_WRITE;
    }
    break;

  case PRN_WRITE:
    USBH_PRN_Send(phost);
    printerController = 0;
    PRN_Handle->state = PRN_IDLE;
    break;

  default:
    break;
  }

  return error;
}

static USBH_StatusTypeDef USBH_PRN_SOFProcess(USBH_HandleTypeDef *phost)
{

  return USBH_OK;
}

uint8_t USBH_PRN_IsReady(USBH_HandleTypeDef *phost)
{
  PRN_HandleTypeDef *PRN_Handle = (PRN_HandleTypeDef *)phost->pActiveClass->pData;
  uint8_t res;

  if ((phost->gState == HOST_CLASS) && (PRN_Handle->state == PRN_IDLE))
  {
    res = 1U;
  }
  else
  {
    res = 0U;
  }

  return res;
}

/**
  * @brief: Funcao que envia prepara buffer e inicia envio para impressora
  * @param buffer: buffer em formato PCL para envio para impressora 
  * @param size: tamanho do buffer 
  */
void USB_Send2Printer(uint8_t *buffer, uint16_t size)
{
  printerController = 1;
  memset(bufferString, 0, MAX_BUFFER);

  // TODO - Verificar erro
  if (size < MAX_BUFFER)
  {
    memcpy(bufferString, buffer, size);
  }
}

/**
  * @brief: Funcao auxiliar para fazer prepend de uma string 
  * @param s: String que sera modificada 
  * @param t: String que sera adicionada no inicio da string final
  */
static void prepend(char *s, const char *t)
{
  size_t len = strlen(t);
  memmove(s + len, s, strlen(s) + 1);
  memcpy(s, t, len);
}

/**
  * @brief: Adiciona cabecalhos PJL no buffer em PCL 
  */
static void addPJLHeaders()
{
  prepend(bufferString, pjl_Header);
  size_t len = strlen(pjl_Footer);
  memcpy(bufferString + MAX_BUFFER - len - 1, pjl_Footer, len);
}

/**
  * @brief: Funcao que adiconar cabecalhos e dados para impressora 
  * @param phost: Handler USB 
  */
void USBH_PRN_Send(USBH_HandleTypeDef *phost)
{

  //    addPJLHeaders();

  uint8_t buff[MAX_BUFFER];
  memcpy(buff, bufferString, MAX_BUFFER);

  PRN_HandleTypeDef *PRN_Handle = (PRN_HandleTypeDef *)phost->pActiveClass->pData;

  uint8_t pkg[USB_PKG_SIZE] = {0};
  for (int i = 0; i < MAX_BUFFER; i = i + USB_PKG_SIZE)
  {
    memset(pkg, 0, USB_PKG_SIZE);
    memcpy(pkg, buff + i, USB_PKG_SIZE);
    USBH_BulkSendData(phost, pkg, USB_PKG_SIZE, PRN_Handle->OutPipe, 1U);
    HAL_Delay(2);
  }

  uint8_t URB_Status = USBH_URB_NYET;

  uint16_t timeout = 10000;

  while (URB_Status != USBH_URB_DONE)
  {
    URB_Status = USBH_LL_GetURBState(phost, PRN_Handle->OutPipe);
    timeout--;
    if (timeout <= 0)
      break;
  }
}

/* NOT YET IMPLEMENTED - GET ADDITIONAL INFO FROM PRINTER*/
/*

static USBH_StatusTypeDef USBH_PRN_GET_DEVICE_ID(USBH_HandleTypeDef *phost)
{
  uint16_t length = 0x09U;
  phost->Control.setup.b.bmRequestType = 0b10100001;

  phost->Control.setup.b.bRequest = 0;
  phost->Control.setup.b.wValue.w = 0;
  phost->Control.setup.b.wIndex.w = 0;
  phost->Control.setup.b.wLength.w = 0;

  uint8_t pData[9];

  return USBH_CtlReq(phost, pData, length);
}

static USBH_StatusTypeDef USBH_PRN_SOFT_RESET(USBH_HandleTypeDef *phost)
{
  phost->Control.setup.b.bmRequestType = 0b0010000;
  phost->Control.setup.b.bRequest = 2;
  phost->Control.setup.b.wValue.w = 0;
  phost->Control.setup.b.wIndex.w = 0;
  phost->Control.setup.b.wLength.w = 0;

  return USBH_CtlReq(phost, 0, 0);
}

static USBH_StatusTypeDef USBH_PRN_GET_STATUS_PORT(USBH_HandleTypeDef *phost)
{
  phost->Control.setup.b.bmRequestType =  0b10100001;

  phost->Control.setup.b.bRequest = 1;
  phost->Control.setup.b.wValue.w = 0;
  phost->Control.setup.b.wIndex.w = 0;
  phost->Control.setup.b.wLength.w = 1;

  return USBH_CtlReq(phost, 0, 0);
}

static USBH_StatusTypeDef USBH_PRN_GET_STATUS(USBH_HandleTypeDef *phost)
{
  phost->Control.setup.b.bmRequestType =  0xB0;

  phost->Control.setup.b.bRequest = 0;
  phost->Control.setup.b.wValue.w = 0;
  phost->Control.setup.b.wIndex.w = 0;
  phost->Control.setup.b.wLength.w = 2;

  return USBH_CtlReq(phost, 0, 0);
}
*/
