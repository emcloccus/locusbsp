#include "stm32f4xx_hal.h"

#include "FT5X06Touch.h"

#define FT5X06_I2C_ADDR  (0x70 << 1)

#define DEVICE_STATUS 		  0x00
#define FTS_REG_CHIP_ID       0xA3	/* chip ID */
#define FTS_REG_FW_VER        0xA6	/* FW  version */
#define FTS_REG_VENDOR_ID     0xA8	/* TP vendor ID */
#define FTS_REG_POINT_RATE    0x88	/* report rate */

FT5X06_T g_tFT5X06;

void FT5X06Init(I2C_HandleTypeDef *hi2c1)
{

	  uint8_t aRxBuffer;
	  uint8_t res;

	  uint16_t deviceAddress = FT5X06_I2C_ADDR;

	  res =  HAL_I2C_Master_Receive(hi2c1, 0x08 << 1, aRxBuffer, 8, 100);

	  /*
	  uint8_t addr = 0;

	  for( int i = 0 ; i < 0x7f; i ++) {
		  res = HAL_I2C_IsDeviceReady(hi2c1, i << 1, 1, 100);
		  if( res == HAL_OK) {
			  addr = i;
		  }
	  }
	  */

	  g_tFT5X06.TimerCount = 0;
	  g_tFT5X06.Enable = 1;
	  g_tFT5X06.State = 0;

}

FT5X06_T GetState()
{
	FT5X06_T touch = g_tFT5X06;
	return g_tFT5X06;
}


void ClearState()
{
	g_tFT5X06.State = 0;
}

uint8_t FT5X06_Read(I2C_HandleTypeDef *hi2c1)
{
	int res = 0;
    uint8_t buf[GFG_POINT_READ_BUF];


    HAL_I2C_Mem_Read(hi2c1, 0x38<<1, 0x02, 1, buf, 1, 200);
	/* TODO - ?? verificar se foi press down ou up ??? */

	HAL_I2C_Mem_Read(hi2c1, 0x38<<1, 0x00, 1, buf, 33, 200);

	for (uint8_t i = 0; i < FT5X06_TOUCH_POINTS; i ++) {

		uint8_t pointid;

		pointid = (buf[5 + 6*i]) >> 4;
		if (pointid >= 0x0f)
			{
				break;
			}
			else
			{
	        	g_tFT5X06.X[i] = (int16_t)(buf[3 + 6*i] & 0x0F)<<8 | (int16_t)buf[4 + 6*i];
	        	g_tFT5X06.Y[i] = (int16_t)(buf[5 + 6*i] & 0x0F)<<8 | (int16_t)buf[6 + 6*i];
	        	g_tFT5X06.Event[i] = buf[0x3 + 6*i] >> 6;
	        	g_tFT5X06.id[i] = (buf[5 + 6*i])>>4;

	        	g_tFT5X06.State = 1;
	    	}

	}

	uint16_t x = g_tFT5X06.X[0];
	uint16_t y = g_tFT5X06.Y[0];

	if (x > 799)
	{
		x = 799;
	}
	if (y > 479)
	{
		y = 479;
	}


	return buf;
}



uint8_t FT5X06_ReadID(void)
{
	uint8_t id;

	/*
	FT5X06_ReadReg(FTS_REG_CHIP_ID, &id, 1);

	g_tFT5X06.ChipID = id;
	*/
	return id;
}

